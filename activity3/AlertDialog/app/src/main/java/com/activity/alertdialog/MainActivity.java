package com.activity.alertdialog;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText Input = findViewById(R.id.et_edtext);
        Button Show = findViewById(R.id.bt_clickme);

        Show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Text = Input.getText().toString();
                if (Text.isEmpty()) {
                    alert("Please Enter Text!");
                }else{
                    alert(Text);
                }

            }
        });
    }
    private void alert (String message){
        AlertDialog rs = new AlertDialog.Builder(MainActivity.this)
                .setTitle("Message")
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialog, int which){
                      dialog.dismiss();
            }
        })
                .create();
                rs.show();

    }

}